package com.mhp.coding.challenges.mapping.mappers;

import com.mhp.coding.challenges.mapping.models.db.Article;
import com.mhp.coding.challenges.mapping.models.dto.ArticleDto;

public interface ArticleMapperInterface {
    public ArticleDto map(Article article);

    public Article map(ArticleDto articleDto);
}
