package com.mhp.coding.challenges.mapping.mappers;

import com.mhp.coding.challenges.mapping.models.db.Article;
import com.mhp.coding.challenges.mapping.models.db.blocks.ArticleBlock;
import com.mhp.coding.challenges.mapping.models.db.blocks.TextBlock;
import com.mhp.coding.challenges.mapping.models.db.blocks.VideoBlock;
import com.mhp.coding.challenges.mapping.models.db.blocks.VideoBlockType;
import com.mhp.coding.challenges.mapping.models.dto.ArticleDto;
import com.mhp.coding.challenges.mapping.models.dto.blocks.ArticleBlockDto;
import nl.altindag.log.LogCaptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class ArticleMapperTest {

    @Test
    public void mapArticleToArticleDtoTest() {
        ArticleMapper articleMapper = new ArticleMapper(new ModelMapperConfiguration().modelMapper());
        Article article = new Article();

        Long id = 159L;
        String title = "MHP bible";
        String author = "Hans Werner";
        String description = "Collection a holy texts";
        Set<ArticleBlock> articleBlockSet = createBlocks(id);

        article.setId(id);
        article.setTitle(title);
        article.setAuthor(author);
        article.setDescription(description);
        article.setBlocks(articleBlockSet);
        ArticleDto articleDto = articleMapper.map(article);

        assertEquals("id have to be: " + id.longValue() + " but it was: " + articleDto.getId().longValue(), articleDto.getId().longValue(), id.longValue());
        assertTrue("title have to be: " + title + " but it was:" + articleDto.getTitle(), articleDto.getTitle().equals(title));
        assertTrue("author have to be: " + author + " but it was:" + articleDto.getAuthor(), articleDto.getAuthor().equals(author));
        assertTrue("description have to be: " + description + " but it was:" + articleDto.getDescription(), articleDto.getDescription().equals(description));
        assertEquals("blocksize have to be: " + articleBlockSet.size() + " but it was: " + articleDto.getBlocks().size(), articleDto.getBlocks().size(), articleBlockSet.size());
        assertTrue("ArticleBlockDtos are not sorted: " + articleDto.getBlocks(), isSorted(articleDto.getBlocks()));
    }

    /**
     * This test is checking for an error message. This error message will only show up if there is no mapper for a subclasses from ArticleBlock
     */
    @Test
    public void mapArticleToArticleDtoTestNewSubClassFromArticleBlockDto() {
        LogCaptor logCaptor = LogCaptor.forClass(ModelMapperConfiguration.class);
        ArticleMapper articleMapper = new ArticleMapper(new ModelMapperConfiguration().modelMapper());
        Article article = new Article();

        Long id = 159L;
        Set<ArticleBlock> articleBlockSet = createBlocks(id);
        articleBlockSet.add(new ArticleBlock() {
        });
        article.setId(id);
        article.setBlocks(articleBlockSet);
        ArticleDto articleDto = articleMapper.map(article);

        assertEquals("id have to be: " + id.longValue() + " but it was: " + articleDto.getId().longValue(), articleDto.getId().longValue(), id.longValue());
        assertEquals("blocksize have to be: " + articleBlockSet.size() + " but it was: " + articleDto.getBlocks().size() + 1, articleDto.getBlocks().size() + 1, articleBlockSet.size());
        assertTrue("ArticleBlockDtos are not sorted: " + articleDto.getBlocks(), isSorted(articleDto.getBlocks()));
        assertTrue(logCaptor.getErrorLogs().get(0).contains("No Mapping found for: class com.mhp.coding.challenges.mapping.mappers"));
    }

    private Set<ArticleBlock> createBlocks(Long articleId) {
        final Set<ArticleBlock> result = new HashSet<>();

        final TextBlock textBlock = new TextBlock();
        textBlock.setText("Some Text for " + articleId);
        textBlock.setSortIndex(0);
        result.add(textBlock);

        final VideoBlock videoBlock = new VideoBlock();
        videoBlock.setType(VideoBlockType.YOUTUBE);
        videoBlock.setUrl("https://youtu.be/myvideo");
        videoBlock.setSortIndex(5);
        result.add(videoBlock);

        final TextBlock secondTextBlock = new TextBlock();
        secondTextBlock.setText("Second Text for " + articleId);
        secondTextBlock.setSortIndex(2);
        result.add(secondTextBlock);
        return result;
    }

    private boolean isSorted(Collection<ArticleBlockDto> articleBlockDtos) {
        int sortedIndexpointer = 0;
        for (ArticleBlockDto articleBlockDto : articleBlockDtos) {
            if (articleBlockDto.getSortIndex() < sortedIndexpointer) {
                return false;
            }
            if (articleBlockDto.getSortIndex() > sortedIndexpointer) {
                sortedIndexpointer = articleBlockDto.getSortIndex();
            }
        }
        return true;
    }

}
