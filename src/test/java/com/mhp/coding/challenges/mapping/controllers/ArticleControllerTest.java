package com.mhp.coding.challenges.mapping.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mhp.coding.challenges.mapping.models.dto.ArticleDto;
import com.mhp.coding.challenges.mapping.services.ArticleService;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(JUnitParamsRunner.class)
@WebMvcTest(ArticleController.class)
public class ArticleControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ArticleService articleService;
    @ClassRule
    public static final SpringClassRule scr = new SpringClassRule();

    @Rule
    public final SpringMethodRule smr = new SpringMethodRule();

    @Test
    @Parameters({"0", "10", "159"})
    public void listTest(int itemAmount) throws Exception {
        ArticleDto articleDto = new ArticleDto();
        articleDto.setId(100L);
        List<ArticleDto> articleDtoList = new ArrayList<>();
        IntStream.range(0, itemAmount).forEach(value -> articleDtoList.add(articleDto));
        when(articleService.list()).thenReturn(articleDtoList);
        mockMvc.perform(get("/article"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(itemAmount)));
    }

    @Test
    @Parameters({"-2147483648", "-99999", "-10", "-1", "0", "1", "10", "999999", "2147483648"})
    public void detailsTest(Long articleId) throws Exception {
        ArticleDto articleDto = new ArticleDto();
        articleDto.setId(articleId);
        when(articleService.articleForId(articleId)).thenReturn(articleDto);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValueAsString(articleDto);

        //this part is needed because jsonPath function mapped it to int instead of long
        Object result;
        try {
            result = Integer.parseInt(articleId.toString());
        } catch (NumberFormatException exception) {
            result = Long.parseLong(articleId.toString());
        }

        mockMvc.perform(get("/article/" + articleId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['id']", is(result)));
    }

    @Test
    public void detailsTestFailure() throws Exception {
        Long articleId = 123L;
        when(articleService.articleForId(articleId)).thenThrow(new RuntimeException());
        mockMvc.perform(get("/article/" + articleId))
                .andExpect(status().isNotFound());
    }
}
