package com.mhp.coding.challenges.mapping.services;

import com.mhp.coding.challenges.mapping.mappers.ArticleMapperInterface;
import com.mhp.coding.challenges.mapping.models.db.Article;
import com.mhp.coding.challenges.mapping.models.dto.ArticleDto;
import com.mhp.coding.challenges.mapping.repositories.ArticleRepositoryInterface;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ArticleServiceTest {

    @MockBean
    private ArticleRepositoryInterface articleRepository;
    @MockBean
    private ArticleMapperInterface articleMapper;

    ArticleService articleService;

    @Before
    public void init() {
        articleService = new ArticleService(articleRepository, articleMapper);
    }

    @Test
    public void listTest() {
        Article article = new Article();
        article.setId(123L);
        when(articleRepository.all()).thenReturn(Arrays.asList(article));
        ArticleDto articleDto = new ArticleDto();
        articleDto.setId(456L);
        when(articleMapper.map(article)).thenReturn(articleDto);
        List<ArticleDto> articleDtoList = articleService.list();
        assertEquals(1, articleDtoList.size());
        assertEquals(456L, articleDtoList.stream().findFirst().orElseThrow(RuntimeException::new).getId().longValue());
    }

    @Test
    public void listTestEmptyList() {
        Article article = new Article();
        article.setId(123L);
        when(articleRepository.all()).thenReturn(Collections.emptyList());
        List<ArticleDto> articleDtoList = articleService.list();
        assertEquals(0, articleDtoList.size());
    }

    @Test
    public void articleForIdTest() {
        Long id = 111L;
        Article article = new Article();
        article.setId(110L);
        when(articleRepository.findBy(id)).thenReturn(Optional.of(article));
        ArticleDto articleDto = new ArticleDto();
        Long articleDtoId = 112L;
        articleDto.setId(articleDtoId);
        when(articleMapper.map(article)).thenReturn(articleDto);
        ArticleDto articleDtoResult = articleService.articleForId(id);
        assertEquals(articleDtoResult.getId().longValue(), articleDtoId.longValue());
    }

    @Test(expected = RuntimeException.class)
    public void articleForIdTestFailure() throws Exception {
        Long id = 789L;
        when(articleRepository.findBy(id)).thenReturn(Optional.empty());
        ArticleDto articleDto = articleService.articleForId(id);
        throw new Exception("It should never run to this point");
    }

}
