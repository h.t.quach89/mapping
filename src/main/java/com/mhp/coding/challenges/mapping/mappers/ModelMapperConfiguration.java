package com.mhp.coding.challenges.mapping.mappers;

import com.mhp.coding.challenges.mapping.models.db.Article;
import com.mhp.coding.challenges.mapping.models.db.blocks.*;
import com.mhp.coding.challenges.mapping.models.dto.ArticleDto;
import com.mhp.coding.challenges.mapping.models.dto.ImageDto;
import com.mhp.coding.challenges.mapping.models.dto.blocks.*;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.spi.MappingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.*;
import java.util.stream.Collectors;

@Configuration
public class ModelMapperConfiguration {
    private static Logger logger = LoggerFactory.getLogger(ModelMapperConfiguration.class);
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        Converter<Set<ArticleBlock>, List<ArticleBlockDto>> articleBlockToArticleBlockDtoConverter = new Converter<Set<ArticleBlock>, List<ArticleBlockDto>>() {
            @Override
            public List<ArticleBlockDto> convert(MappingContext<Set<ArticleBlock>, List<ArticleBlockDto>> context) {
                Set<ArticleBlock> articleBlockSet = context.getSource();
                if (articleBlockSet == null) {
                    return Collections.emptyList();
                }
                List<ArticleBlockDto> articleBlockDtoList = new LinkedList<>();
                for (ArticleBlock articleBlock : articleBlockSet) {

                    if (articleBlock instanceof GalleryBlock) {
                        GalleryBlock galleryBlock = (GalleryBlock) articleBlock;
                        List<ImageDto> imageDtoList = galleryBlock.getImages().stream()
                                .map(image -> modelMapper.map(image, ImageDto.class))
                                .collect(Collectors.toList());

                        articleBlockDtoList.add(GalleryBlockDto.builder()
                                .sortIndex(galleryBlock.getSortIndex())
                                .images(imageDtoList)
                                .build());
                    } else if (articleBlock instanceof ImageBlock) {
                        ImageBlock imageBlock = (ImageBlock) articleBlock;
                        articleBlockDtoList.add(ImageBlockDto.builder()
                                .sortIndex(imageBlock.getSortIndex())
                                .image(modelMapper.map(imageBlock.getImage(), ImageDto.class))
                                .build());
                    } else if (articleBlock instanceof TextBlock) {
                        TextBlock textBlock = (TextBlock) articleBlock;
                        articleBlockDtoList.add(TextBlockDto.builder()
                                .sortIndex(textBlock.getSortIndex())
                                .text(textBlock.getText())
                                .build());
                    } else if (articleBlock instanceof VideoBlock) {
                        VideoBlock videoBlock = (VideoBlock) articleBlock;
                        articleBlockDtoList.add(VideoBlockDto.builder()
                                .sortIndex(videoBlock.getSortIndex())
                                .url(videoBlock.getUrl())
                                .type(videoBlock.getType())
                                .build());
                    } else {
                        logger.error("No Mapping found for: " + articleBlock.getClass());
//                        throw new RuntimeException("No Mapping found for: " + articleBlock.getClass()); //Alternative, je nach Use-Case
                    }
                }
                return articleBlockDtoList.stream()
                        .sorted(Comparator.comparing(ArticleBlockDto::getSortIndex))
                        .collect(Collectors.toList());
            }
        };
        TypeMap<Article, ArticleDto> ArticleToArticleDtoTypeMap = modelMapper.createTypeMap(Article.class, ArticleDto.class);
        ArticleToArticleDtoTypeMap.addMappings(mapper -> mapper.using(articleBlockToArticleBlockDtoConverter).map(Article::getBlocks, ArticleDto::setBlocks));
        return modelMapper;
    }
}
