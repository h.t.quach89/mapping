package com.mhp.coding.challenges.mapping.repositories;

import com.mhp.coding.challenges.mapping.models.db.Article;

import java.util.List;
import java.util.Optional;

public interface ArticleRepositoryInterface {
    public List<Article> all();

    public Optional<Article> findBy(Long id);

    public void create(Article article);
}
