package com.mhp.coding.challenges.mapping.services;

import com.mhp.coding.challenges.mapping.mappers.ArticleMapperInterface;
import com.mhp.coding.challenges.mapping.models.db.Article;
import com.mhp.coding.challenges.mapping.models.dto.ArticleDto;
import com.mhp.coding.challenges.mapping.repositories.ArticleRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ArticleService {

    private final ArticleRepositoryInterface repository;

    private final ArticleMapperInterface mapper;

    @Autowired
    public ArticleService(ArticleRepositoryInterface repository, ArticleMapperInterface mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<ArticleDto> list() {
        final List<Article> articles = repository.all();
        if(articles.isEmpty()){
            return Collections.emptyList();
        }
        return articles.stream().map(article -> mapper.map(article)).collect(Collectors.toList());

    }

    public ArticleDto articleForId(Long id) {
        final Optional<Article> optionalArticle = repository.findBy(id);
        Article article = optionalArticle.orElseThrow(RuntimeException::new);
        return mapper.map(article);
    }

    public ArticleDto create(ArticleDto articleDto) {
        final Article create = mapper.map(articleDto);
        repository.create(create);
        return mapper.map(create);
    }
}
